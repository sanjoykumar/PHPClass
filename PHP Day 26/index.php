<?php 
include_once 'src/car.php';

$Car= new car();


echo '<br/>'. "Properties value access from class.". "<br/>";
echo "----------------------------------------------------------------". "<br/>";
echo $Car->door;
echo "<br/>";


echo '<br/>'. "Functions value access from class.". "<br/>";
echo "----------------------------------------------------------------". "<br/>";
echo $Car->sayHello();
echo "<br/>";


echo '<br/>'. "Try to access Object value from class.". "<br/>";
echo "----------------------------------------------------------------". "<br/>";
echo "<br/>". $Car;               // echo object value
echo "<br/>";



echo '<br/>'. "Try to access undined value from class:". "<br/>";
echo "----------------------------------------------------------------". "<br/>";
print $Car->foo;  // __get() function


echo '<br/>'. "Try to access undefined value from class and set the value 'Hello':". "<br/>";
echo "----------------------------------------------------------------". "<br/>";
print $Car->foo1='Hello';   // __set() function
echo "<br/>";


echo '<br/>'. "Try to access undefined function with arguments from class :". "<br/>";
echo "----------------------------------------------------------------". "<br/>";
$Car->meow("foo", "bar", "baz");
echo "<br/>";


echo '<br/>'. "Try to access serialized function with arguments from class :". "<br/>";
echo "----------------------------------------------------------------". "<br/>";
$Car = new car('bla bla', 'a');
$Car->show();
echo "<br />\n";
$s = serialize($Car);
echo $s ."<br />\n";
echo "<br/>";

?>
