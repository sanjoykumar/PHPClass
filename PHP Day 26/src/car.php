<?php

class car {

    public $door = 4;
    protected $window = 6;
    private $seat = 2;
    //public $foo;
    public $foo1;
    public $name = 'TOYOTA';

    function __construct() {
        echo '<br />';
        $this->door = '8: change from constructor.';
        echo "<br />";
    }

    function __destruct() {
        echo '<br />' . "Destructor function is calling from class after excuting PHP scripts." . '<br />';
        echo"I am dying my seat number is :" . $this->seat . '<br />';
    }

    public function __get($prop) {

        echo "Get:$prop";
        echo '<br />';
        echo "You are tring to access a non exist properties " . "<b>" . $prop . "</b>" . " and failed...\n";
        echo '<br />';
    }

    public function __set($prop, $value) {

        echo "Set:$prop to $value";
        //$this->$prop = $value;
    }

    public function __call($function, $args) {
        $args = implode(', ', $args);
        print "Call to $function() with args '$args' failed!\n";
    }

    public function __toString() {
        return $this->name;
    }

    function __sleep() {
        echo 'Going to sleep...';
        return array('1', '2', '3');
    }

    public function sayHello() {

        echo "<br/>" . ' Hello Function.................';
    }

      public function show()
    {
        echo $this->name;
    }
}
