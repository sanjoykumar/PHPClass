<!DOCTYPE html>
<html>
    <head>
        <title>PHP Batch-02 Lab Examination</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <section>

            <h1>Lab Test PHP Batch 02, SEIP ID:100798</h1>

            
            <h3>Test No 2:</h3>
            <form action="process.php" method="post">
                <fieldset>
                    <legend>Log in Information:</legend>

                    <ul>
                        <li>
                            <label for="fullName">Full Name:</label>
                            <input type="text" name="fullname" id="fullName" />
                        </li>

                        <li>
                            <label for="email">E-mail:</label>
                            <input type="email" name="email" id="email" />
                        </li>


                        <li>
                            <label for="password">Password:</label>
                            <input type="password" name="password" id="password" />
                        </li>


                    </ul>
                    <input type="submit" value="Login" />
                </fieldset>	
            </form>

        </section>
    </body>
</html>
