<!DOCTYPE html>

<html>
    <head>
        <title>Simple Calculator</title>
    </head>
    <body>
        <form action="" method="POST">
            <table align="center" border="1">
                <tr>
                    <td>First Number : </td>
                    <td>
                        <input type="text" name="number1" >
                    </td>
                </tr>
                <tr>
                    <td>Second Number : </td>
                    <td>
                        <input type="text" name="number2">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" name="add" value="Add">
                        <input type="submit" name="sub" value="Substract">
                        <input type="submit" name="mul" value="Multiply">
                        <input type="submit" name="div" value="Devide">
                    </td>
                </tr>
                <tr>
                    <td>Result : </td>

                    <td>
                        <?php
                        if (isset($_POST['add']) == 'Add') {
                            echo $_POST['number1'] + $_POST['number2'];
                        }
                        if (isset($_POST['sub']) == 'Substract') {
                            echo $_POST['number1'] - $_POST['number2'];
                        }
                        if (isset($_POST['mul']) == 'Multiply') {
                            echo $_POST['number1'] * $_POST['number2'];
                        }
                        if (isset($_POST['div']) == 'Devide') {
                            echo $_POST['number1'] / $_POST['number2'];
                        }
                        ?>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>

