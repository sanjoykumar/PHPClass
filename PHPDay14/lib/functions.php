<?php

//function definition
function debug($data) {
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

//index.php

function GetAllData() {

    if (array_key_exists('mysessiondata', $_SESSION)) {

        return $_SESSION['mysessiondata'];
    } else {

        return '';
    }
}

//create.php --- To store all data in Session

function Store($data = null) {

    if (is_null($data)) {
        header('location:create.html');
    }

    if (!array_key_exists('mysessiondata', $_SESSION)) {
        $_SESSION['mysessiondata'] = array();
    }

    if (array_key_exists('email', $data) && !empty($data['email'])) {
        $_SESSION['mysessiondata'][] = $data['email'];
        $msg = "A new email has been stored into session.";
        $_SESSION['message'] = $msg;
        header('location:index.php');
    } else {
        $msg = 'No email found, please try again.';
        $_SESSION['message'] = $msg;
        header('location:create.html');
    }
}

//show.php 

function find($id = null) {
    if (is_null($id)) {
        return '';
    }
    return $_SESSION['mysessiondata'][$id];
}


//Edit


function edit(){
    
    if (array_key_exists('email', $_POST) && !empty($_POST['email'])) {


    $_SESSION['mysessiondata'][$_POST['id']] = $_POST['email'];
    $msg = "An  email has been edited into session.";
    $_SESSION['message'] = $msg;
    header('location:index.php');
}

}


//delete


function delete($id) {

    if (array_key_exists('mysessiondata', $_SESSION)) {
        unset($_SESSION['mysessiondata'] [$id]);
    }
    header('location:index.php');
}
