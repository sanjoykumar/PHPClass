-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 20, 2015 at 12:44 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `planet`
--

-- --------------------------------------------------------

--
-- Table structure for table `planet_city`
--

CREATE TABLE IF NOT EXISTS `planet_city` (
`ID` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `CountryCode` varchar(3) NOT NULL,
  `District` varchar(20) NOT NULL,
  `Population` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `planet_city`
--

INSERT INTO `planet_city` (`ID`, `Name`, `CountryCode`, `District`, `Population`) VALUES
(1, 'Kabul', 'AFG', 'Kabul', 1780000),
(2, 'Amsterdam', 'NLD', 'Nooral-Holland', 73200),
(3, 'Haag', 'NLD', 'Zuid-Holland', 593321),
(4, 'Utrect', 'NLD', 'Utrect', 234323),
(5, 'Alzer', 'DZA', 'Alger', 2168000),
(6, 'Tianet', 'DZA', 'Tiaret', 100118),
(7, 'Tafuna', 'ASM', 'Tutulia', 5200),
(8, 'Luanda', 'AGO', 'Luanda', 2022000),
(9, 'Lolito', 'AGO', 'Bengulia', 130000),
(10, 'Sharjah', 'ARE', 'Shajah', 320095),
(11, 'San Miguee', 'ARG', 'Buenas', 248700),
(12, 'Resistenia', 'ARG', 'Chaco', 229212),
(13, 'La Rioja', 'ARG', 'LaRioja', 138117),
(14, 'Hebron', 'PSE', 'Habron', 119401),
(15, 'Rafah', 'PSE', 'Rafah', 92020),
(16, 'Cary', 'USA', 'North Carolina', 91213),
(17, 'Elgin', 'USA', 'Illinois', 89408),
(18, 'Harare', 'ZWE', 'Bulawayo', 621742),
(19, 'Gaza', 'PSE', 'Gaza', 353632),
(20, 'Khan Yunis', 'PSE', 'Khan Yunis', 123175);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `planet_city`
--
ALTER TABLE `planet_city`
 ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `planet_city`
--
ALTER TABLE `planet_city`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
